package br.knin.project.catalogo.video.adapter.rest;

import br.knin.project.catalogo.video.adapter.database.VideosRepository;
import br.knin.project.catalogo.video.adapter.rabbitmq.VideoAvailableChannel;
import br.knin.project.catalogo.video.adapter.rabbitmq.VideoCaptureChannel;
import br.knin.project.catalogo.video.domain.Channel;
import br.knin.project.catalogo.video.domain.VideoSourceEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class VideoSourceConfiguration {

    @Bean
    Channel<VideoSourceEvent> createChannel(final RabbitTemplate template, final ObjectMapper objectMapper) {
        return new VideoCaptureChannel(objectMapper, template);
    }

    @Bean
    VideoAvailableChannel videoAvailableChannel(final VideosRepository repository, final ObjectMapper objectMapper) {
        return new VideoAvailableChannel(repository, objectMapper);
    }

}
