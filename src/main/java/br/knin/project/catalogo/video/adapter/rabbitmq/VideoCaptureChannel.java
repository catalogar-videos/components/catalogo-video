package br.knin.project.catalogo.video.adapter.rabbitmq;

import br.knin.project.catalogo.video.domain.Channel;
import br.knin.project.catalogo.video.domain.VideoSourceEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

public final class VideoCaptureChannel implements Channel<VideoSourceEvent> {

    public VideoCaptureChannel(final ObjectMapper objectMapper, final RabbitTemplate rabbitTemplate) {
        this.objectMapper = objectMapper;
        this.rabbitTemplate = rabbitTemplate;
    }

    private final ObjectMapper objectMapper;

    private final RabbitTemplate rabbitTemplate;

    @SneakyThrows
    @Override
    public void send(final VideoSourceEvent videoSourceEvent) {
        rabbitTemplate.convertAndSend("", "queue.captura.input", objectMapper.writeValueAsBytes(videoSourceEvent));
    }

}
