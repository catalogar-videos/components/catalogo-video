package br.knin.project.catalogo.video.adapter.rest;

import br.knin.project.catalogo.video.domain.ParameterProxy;
import br.knin.project.catalogo.video.adapter.database.VideoSourceRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.stream.Collectors;

@RestController
@RequestMapping("catalog/videos/parameters")
public class ParameterController {

    public ParameterController(final VideoSourceRepository repository) {
        this.repository = repository;
    }

    private final VideoSourceRepository repository;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Collection<ParameterProxy> allParameters() {
        return repository.findAll()
                .stream()
                .map(VideoSourceRepository.VideoSource::toParameterProxy)
                .collect(Collectors.toSet());
    }

}
