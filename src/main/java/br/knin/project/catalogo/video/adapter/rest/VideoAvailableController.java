package br.knin.project.catalogo.video.adapter.rest;

import br.knin.project.catalogo.video.adapter.database.VideosRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("catalog/videos")
public class VideoAvailableController {

    public VideoAvailableController(final VideosRepository repository) {
        this.repository = repository;
    }

    private final VideosRepository repository;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<VideosRepository.VideoEntity> videoCapture
            (
                    @RequestParam(defaultValue = "0") int page,
                    @RequestParam(defaultValue = "10") int size,
                    @RequestParam(defaultValue = "createDate") String sortBy,
                    @RequestParam(defaultValue = "desc") String sortDirection
            ) {

        final Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);

        final Pageable pageable = PageRequest.of(page, size, sort);

        return repository.findAll(pageable);

    }
}
