package br.knin.project.catalogo.video.adapter.rest;

import br.knin.project.catalogo.video.domain.Channel;
import br.knin.project.catalogo.video.domain.VideoSourceEvent;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("catalog/videos")
public class CaptureController {

    public CaptureController(final Channel<? super VideoSourceEvent> channel) {
        this.channel = channel;
    }

    private final Channel<? super VideoSourceEvent> channel;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void videoCapture(@RequestBody final VideoCapturePayload payload) {
        payload.send(channel);
    }

    @ToString
    @NoArgsConstructor
    @Setter
    public static final class VideoCapturePayload {

        private String alias;

        private Map<String, String> parameters;

        private void send(final Channel<? super VideoSourceEvent> channel) {

            channel.send
                    (
                            new VideoSourceEvent() {

                                @Override
                                public String getVideoSource() {
                                    return alias;
                                }

                                @Override
                                public Map<String, String> getParameters() {
                                    return parameters;
                                }

                            }
                    );

        }
    }
}
