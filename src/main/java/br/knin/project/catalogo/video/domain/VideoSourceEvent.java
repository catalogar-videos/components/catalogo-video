package br.knin.project.catalogo.video.domain;

import java.util.Map;

@SuppressWarnings("unused")
public interface VideoSourceEvent {

    String getVideoSource();

    Map<String, String> getParameters();

}
