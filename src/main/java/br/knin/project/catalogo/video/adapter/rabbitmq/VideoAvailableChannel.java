package br.knin.project.catalogo.video.adapter.rabbitmq;

import br.knin.project.catalogo.video.adapter.database.VideosRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Setter;
import lombok.ToString;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

import java.time.LocalDate;
import java.util.Collection;

public class VideoAvailableChannel {

    public VideoAvailableChannel(final VideosRepository repository, final ObjectMapper objectMapper) {
        this.repository = repository;
        this.objectMapper = objectMapper;
    }

    private final VideosRepository repository;

    private final ObjectMapper objectMapper;

    @RabbitListener(queues = "queue.captura.output")
    public void handleMessage(final String string) throws JsonProcessingException {
        final RawPayload raw = objectMapper.readValue(string, RawPayload.class);
        raw.save(repository);
    }

    @ToString
    @Setter
    static class RawPayload {

        private String id;

        private String videoFilePath;

        private MetadataPayload metadata;

        public void save(final VideosRepository repository) {
            repository.save(new VideosRepository.VideoEntity(id, videoFilePath, metadata.create()));
        }

        @Setter
        public static final class MetadataPayload {

            private String name;

            private String author;

            private Collection<String> tags;

            private LocalDate creationDate;

            public VideosRepository.MetadataEntity create() {
                return new VideosRepository.MetadataEntity(author, creationDate, name, tags);
            }
        }

    }
}
