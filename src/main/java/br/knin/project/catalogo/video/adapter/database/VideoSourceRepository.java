package br.knin.project.catalogo.video.adapter.database;

import br.knin.project.catalogo.video.domain.ParameterProxy;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Collection;

public interface VideoSourceRepository extends MongoRepository<VideoSourceRepository.VideoSource, String> {

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Document("video_source")
    class VideoSource {

        @Field("required_parameters")
        private Collection<String> parameters;

        private String alias;

        private String name;

        public ParameterProxy toParameterProxy() {

            return new ParameterProxy() {
                @Override
                public String getAlias() {
                    return alias;
                }

                @Override
                public Collection<String> getParameters() {
                    return parameters;
                }

                @Override
                public String getName() {
                    return name;
                }
            };

        }
    }

}
