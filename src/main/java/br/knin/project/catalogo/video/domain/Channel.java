package br.knin.project.catalogo.video.domain;

public interface Channel<T> {

    void send(final T t);

}
