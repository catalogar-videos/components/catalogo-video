package br.knin.project.catalogo.video.adapter.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDate;
import java.util.Collection;

public interface VideosRepository extends MongoRepository<VideosRepository.VideoEntity, String> {

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Document("videos")
    final class VideoEntity {

        @Id
        private String id;

        private String videoFilePath;

        private MetadataEntity metadata;

    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    final class MetadataEntity {

        private String author;

        private LocalDate createDate;

        private String name;

        private Collection<String> tags;

    }

}
