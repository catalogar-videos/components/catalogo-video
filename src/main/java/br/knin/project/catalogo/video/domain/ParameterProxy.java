package br.knin.project.catalogo.video.domain;

import java.util.Collection;

@SuppressWarnings("unused")
public interface ParameterProxy {

    String getAlias();

    Collection<String> getParameters();

    String getName();

}
